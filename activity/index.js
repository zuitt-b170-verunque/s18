// console.log(`hello world`)


/* 
1. In the S18 folder, create an activity folder and an index.html and script.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
6. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.
8. Create a constructor for creating a pokemon with the following properties:
- Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)
9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
13. Invoke the tackle method of one pokemon object to see if it works as intended.
 */

let trainer = {
    name: "Ash Ketchum",
    age: 10,
	
    friends:{
		hoen : ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	pokemon:["Pikachu", "Charizard", "Balbasaur", "Raichu"],
    talk: function(){
		console.log(`Pikachu I choose you`);
	}
}
console.log(trainer)
// dot notation
console.log (`Result of dot notation`);
console.log(`${trainer.name}`);

// square bracket
console.log (`Result of square bracket notation`);
console.log(trainer.pokemon);

// talk method
console.log (`Result of talk notation`);
console.log(trainer.talk());





function Pokemon(name, level){
	this.name = name,
	this.level = level,
	this.health = 2*level,
	this.attack = level
	// Skills of Pokemon
	this.tackle = function(target){
	console.log(this.name + " tackled " + target.name);
	console.log(target.name +"'s" + " health is now reduced by " + target.health );
	if (target.health === 0){
		this.faint();
	}
	this.faint = function(){
		console.log(target.name + " fainted.")	
	}
	target.health -= target.attack
	}
}

let Pikachu = new Pokemon ("Pikachu", 16);
let Charizard = new Pokemon("Charizard", 8);
let Balbasaur = new Pokemon("Balbasaur", 9);
let Raichu = new Pokemon("Raichu", 16);

console.log(Pikachu)
console.log(Charizard)
console.log(Balbasaur)
console.log(Raichu)

/* console.log(`
First attack

${Charizard.tackle (Pikachu)}
Pikachu's current health is${Pikachu.health}

${Charizard.tackle (Pikachu)}
Pikachu's current health is${Pikachu.health}

`) */

console.log(`First attack`)

console.log(`${Charizard.tackle (Pikachu)} `)
console.log(`Pikachu's current health is ${Pikachu.health}`)

console.log(`${Charizard.tackle (Pikachu)}`)
console.log(`Pikachu's current health is ${Pikachu.health}`)

// critical health
console.log(`${Charizard.tackle (Pikachu)}`)
console.log(`Pikachu's current health is ${Pikachu.health}`)

// defeat
console.log (`${Charizard.faint (Pikachu)}`)

// tally
console.log(Pikachu)



