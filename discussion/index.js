
let grades = [98.5, 94.3, 89.2, 90.0 ]
console.log(grades);

let grade = {
    math:98.5,
    english:94.3,
    science:90.00,
    MAPEH: 89.2
}
console.log(grade)

console.log(grade.math)

let cellphone = {
    brand:"xiaomi",
    color:"white",
    mfd:2022,
}

console.log(cellphone);

let student = {
    firstName: "David",
    lastName: "Verunque",
    mobileNo: 090909889,
    location: {
        city: "tokyo",
        country:"Japan"
    },
    email:["verunque@gmail.com", "email@email.com"],
    fullName:(function name(fullName){
        console.log(this.firstName + " " + this.lastName);
    }
    )
}
// console.log(student.firstName + " " + student.lastName);
student.fullName();

console.log(student.location.city);
console.log(student.email);
console.log(student.email[0]);

let contactlist = [
{
    firstName: "John",
    lastName: "Smith",
    location: "Japan",
},
{
    firstName: "Jane",
    lastName: "Smith",
    location: "Japan",
},
{
    firstName: "Jasmine",
    lastName: "Smith",
    location: "Japan",
}
];

console.log(contactlist[1]);
console.log(contactlist[2].firstName);

function Laptop (name, manufactureDate) {
    this.name = name;
    this.manufactureDate = manufactureDate;
}

let laptop1 = new Laptop ("Lenovo", 2008);

console.log(laptop1);

let laptop2 = new Laptop ("Acer", 2019);
console.log(laptop2);

let car = {};

car.name = "Honda Civic";
console.log(car);

car["manufacturedDate"] = 2020;
console.log(car);

car.name = "volvo";
console.log(car);


delete car.manufacturedDate;
console.log(car);

let person = {
    name:"John",
    talk: function(){
        console.log(`Hi my name is ` + this.name)
    }
}


// add property
person.walk = function(){
	console.log(this.name + " walked 25 steps forward.");
}